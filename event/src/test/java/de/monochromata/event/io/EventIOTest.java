package de.monochromata.event.io;

import static java.util.Arrays.asList;
import static java.util.Collections.emptyList;
import static java.util.Collections.singletonList;
import static java.util.Collections.singletonMap;
import static java.util.stream.Collectors.toList;
import static org.assertj.core.api.Assertions.assertThat;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import org.junit.Test;

import com.fasterxml.jackson.databind.RuntimeJsonMappingException;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;

public class EventIOTest {

	@Test(expected = IOException.class)
	public void attemptingToReadEventsFromNonExistantFileYieldsIOException() throws IOException {
		EventIO.readEventsFrom("doesNotExist.txt", A.class);
	}

	public void attemptingToReadEventsFromEmptyFileDoesNotYieldIOException() throws IOException {
		final File emptyFile = File.createTempFile("file", "tmp");
		try {
			assertThat(emptyFile).exists();
			EventIO.readEventsFrom(emptyFile, A.class).collect(Collectors.toList());
		} finally {
			emptyFile.delete();
		}
	}

	@Test
	public void nonExistantFileIsCreatedBeforeWritingToIt() throws IOException {
		final File nonExistantFile = new File("doesNotExist" + System.currentTimeMillis() + ".data");
		assertThat(nonExistantFile).doesNotExist();

		try (final EventOutput<?> eo = EventIO.writeEventsTo(nonExistantFile, A.class)) {
			assertThat(nonExistantFile).exists();
		} finally {
			nonExistantFile.delete();
		}
	}

	@Test
	public void nonExistantDirectoryIsCreatedBeforeWritingToAFileInIt() throws IOException {
		final File fileInNonExistantDir = new File(
				"noSuchDir" + System.currentTimeMillis() + File.separator + "/file.data");
		final File nonExistantDir = fileInNonExistantDir.getParentFile();

		assertThat(fileInNonExistantDir).doesNotExist();
		assertThat(nonExistantDir).isNotNull();
		assertThat(nonExistantDir).doesNotExist();

		try (final EventOutput<?> eo = EventIO.writeEventsTo(fileInNonExistantDir, A.class)) {
			assertThat(fileInNonExistantDir).exists();
			assertThat(nonExistantDir).isDirectory();
			assertThat(nonExistantDir).exists();
		} finally {
			fileInNonExistantDir.delete();
			nonExistantDir.delete();
		}
	}

	@Test
	public void nonExistantParentDirectoryIsCreatedBeforeWritingToAFileInItsChildDirectory() throws IOException {
		final File fileInNonExistantDir = new File(
				"noSuchDir" + System.currentTimeMillis() + File.separator + "/dir/file.data");
		final File nonExistantSubDir = fileInNonExistantDir.getParentFile();
		final File nonExistantDir = nonExistantSubDir.getParentFile();

		assertThat(fileInNonExistantDir).doesNotExist();
		assertThat(nonExistantSubDir).isNotNull();
		assertThat(nonExistantSubDir).doesNotExist();
		assertThat(nonExistantDir).isNotNull();
		assertThat(nonExistantDir).doesNotExist();

		try (final EventOutput<?> eo = EventIO.writeEventsTo(fileInNonExistantDir, A.class)) {
			assertThat(fileInNonExistantDir).exists();
			assertThat(nonExistantSubDir).isDirectory();
			assertThat(nonExistantSubDir).exists();
			assertThat(nonExistantDir).isDirectory();
			assertThat(nonExistantDir).exists();
		} finally {
			fileInNonExistantDir.delete();
			nonExistantSubDir.delete();
			nonExistantDir.delete();
		}
	}

	@Test
	public void emptyStreamIsCreatedFromFileInitializedForObjectOutput() throws IOException {
		final File file = new File("emptyStream" + System.currentTimeMillis() + ".data");
		try {
			try (final EventOutput<?> eo = EventIO.writeEventsTo(file, A.class)) {
				// don't write anything
			}

			try (final Stream<A> eventInput = EventIO.readEventsFrom(file, A.class)) {
				assertThat(eventInput.collect(toList())).isEmpty();
			}
		} finally {
			file.delete();
		}
	}

	@Test
	public void writtenEventsCanBeRead() throws IOException {
		final File file = new File("rw" + System.currentTimeMillis() + ".data");
		try {
			final Integer int1 = 1;
			final String string1 = "string 1";
			final Double double1 = 1.0d;
			final EventOutput<A> eventOutput = EventIO.writeEventsTo(file, A.class);
			eventOutput.write(new A(new B(1), emptyList()));
			eventOutput.write(new A(new B(2), singletonList(new B(3))));
			eventOutput.write(new A(new B(4), asList(new B(5), new B(6))));
			eventOutput.close();

			final Stream<A> stream = EventIO.readEventsFrom(file, A.class);
			final List<A> list = stream.collect(toList());
			assertThat(list).containsExactly(new A(new B(1), emptyList()), new A(new B(2), singletonList(new B(3))),
					new A(new B(4), asList(new B(5), new B(6))));
		} finally {
			file.delete();
		}
	}

	@Test(expected = NullPointerException.class)
	public void attemptingToWriteNullEventRaisesNullPointerException() throws IOException {
		final File file = new File("nullOutput" + System.currentTimeMillis() + ".data");
		try (final EventOutput<?> eo = EventIO.writeEventsTo(file, A.class)) {
			eo.write(null);
		} finally {
			file.delete();
		}
	}

	@Test(expected = RuntimeJsonMappingException.class)
	public void writeObjectWithMemberOfInterfaceTypeWithoutAnnotationsYieldsException() throws IOException {
		final File file = new File("output1" + System.currentTimeMillis() + ".data");
		try {
			final EventOutput<C> eo = EventIO.writeEventsTo(file, C.class);
			eo.write(new C(new DImpl(10)));
			eo.close();

			final Stream<C> stream = EventIO.readEventsFrom(file, C.class);
			final List<C> list = stream.collect(toList());
			assertThat(list).containsExactly(new C(new DImpl(10)));
		} finally {
			file.delete();
		}
	}

	@Test
	public void writeObjectWithMemberOfInterfaceTypeWithMixinAnnotations() throws IOException {
		final File file = new File("output2" + System.currentTimeMillis() + ".data");
		try {
			final EventOutput<C> eo = EventIO.writeEventsTo(file, C.class);
			eo.write(new C(new DImpl(10)));
			eo.close();

			final Stream<C> stream = EventIO.readEventsFrom(file, C.class, singletonMap(D.class, DMixin.class));
			final List<C> list = stream.collect(toList());
			assertThat(list).containsExactly(new C(new DImpl(10)));
		} finally {
			file.delete();
		}
	}

	@Test(expected = RuntimeJsonMappingException.class)
	public void readSubclassesOfInterfaceForWhichTheWriterIsConfiguredYieldsException() throws IOException {
		final File file = new File("output3" + System.currentTimeMillis() + ".data");
		try {
			final EventOutput<E> eo = EventIO.writeEventsTo(file, E.class);
			eo.write(new E1(100, 101));
			eo.write(new E2(200, 201));
			eo.close();

			final Stream<E> stream = EventIO.readEventsFrom(file, E.class);
			final List<E> list = stream.collect(toList());
			assertThat(list).containsExactly(new E1(100, 101), new E2(200, 201));
		} finally {
			file.delete();
		}
	}

	@Test(expected = IllegalArgumentException.class)
	public void readInvocationWithMixinClassWithoutJacksonDatabindAnnotationsYieldsException() throws IOException {
		final File file = new File("output3" + System.currentTimeMillis() + ".data");
		try {
			final EventOutput<E> eo = EventIO.writeEventsTo(file, E.class);
			eo.write(new E1(100, 101));
			eo.close();

			EventIO.readEventsFrom(file, E.class, singletonMap(E.class, InvalidMixinWithoutDatabindAnnotations.class));
		} finally {
			file.delete();
		}
	}

	private static class A {

		private B b1;
		private List<B> bs;

		/**
		 * Default constructor provided to support serialization.
		 */
		public A() {
		}

		public A(final B b1, final List<B> bs) {
			this.b1 = b1;
			this.bs = bs;
		}

		public B getB1() {
			return b1;
		}

		public List<B> getBs() {
			return bs;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((b1 == null) ? 0 : b1.hashCode());
			result = prime * result + ((bs == null) ? 0 : bs.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			A other = (A) obj;
			if (b1 == null) {
				if (other.b1 != null)
					return false;
			} else if (!b1.equals(other.b1))
				return false;
			if (bs == null) {
				if (other.bs != null)
					return false;
			} else if (!bs.equals(other.bs))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "A [b1=" + b1 + ", bs=" + bs + "]";
		}

	}

	private static class B {

		private Integer int1;

		/**
		 * Default constructor provided to support serialization.
		 */
		public B() {
		}

		public B(final Integer int1) {
			this.int1 = int1;
		}

		public Integer getInt1() {
			return int1;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((int1 == null) ? 0 : int1.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			B other = (B) obj;
			if (int1 == null) {
				if (other.int1 != null)
					return false;
			} else if (!int1.equals(other.int1))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "B [int1=" + int1 + "]";
		}

	}

	private static class C {

		private D d;

		public C() {
		}

		public C(final D d) {
			this.d = d;
		}

		public D getD() {
			return d;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((d == null) ? 0 : d.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			C other = (C) obj;
			if (d == null) {
				if (other.d != null)
					return false;
			} else if (!d.equals(other.d))
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "C [d=" + d + "]";
		}

	}

	private interface D {

		int getE();

	}

	private static class DImpl implements D {

		private int e;

		public DImpl() {
		}

		public DImpl(int e) {
			this.e = e;
		}

		@Override
		public int getE() {
			return e;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + e;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			DImpl other = (DImpl) obj;
			if (e != other.e)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "DImpl [e=" + e + "]";
		}

	}

	@JsonDeserialize(as = DImpl.class)
	public interface DMixin {
	}

	private interface E {
		public int getF();
	}

	private static class E1 implements E {

		private int f;
		private int i;

		public E1() {
		}

		public E1(int f, int i) {
			this.f = f;
			this.i = i;
		}

		@Override
		public int getF() {
			return f;
		}

		public int getI() {
			return i;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + f;
			result = prime * result + i;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			E1 other = (E1) obj;
			if (f != other.f)
				return false;
			if (i != other.i)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "H [f=" + f + ", i=" + i + "]";
		}

	}

	private static class E2 implements E {

		private int f;
		private int j;

		public E2() {
		}

		public E2(int f, int j) {
			this.f = f;
			this.j = j;
		}

		@Override
		public int getF() {
			return f;
		}

		public int getJ() {
			return j;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + f;
			result = prime * result + j;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			E2 other = (E2) obj;
			if (f != other.f)
				return false;
			if (j != other.j)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "I [f=" + f + ", j=" + j + "]";
		}

	}

	@FunctionalInterface
	private interface InvalidMixinWithoutDatabindAnnotations {
		void foo();
	}
}
