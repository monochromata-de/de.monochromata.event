package de.monochromata.event;

/**
 * A tracker tracks events emitted by third parties and can inform listeners
 * about these events.
 * 
 * @param <L>
 *            the type of listeners that can be attached to the tracker.
 */
public interface Tracker<L> {

	void addListener(final L listener);

	void removeListener(final L listener);

}
