package de.monochromata.event.io;

import static java.util.Collections.emptyMap;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.UncheckedIOException;
import java.lang.annotation.Annotation;
import java.util.Map;
import java.util.Objects;
import java.util.Spliterator;
import java.util.Spliterators.AbstractSpliterator;
import java.util.function.Consumer;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.ObjectReader;
import com.fasterxml.jackson.databind.ObjectWriter;
import com.fasterxml.jackson.databind.SequenceWriter;
import com.fasterxml.jackson.dataformat.cbor.CBORFactory;

/**
 * Means of writing events to and reading them from files.
 * 
 * <p>
 * The files written by this class may only be read by this class as their file
 * format is specific to the implementation of this class.
 */
public class EventIO {

	private static final String COM_FASTERXML_JACKSON = "com.fasterxml.jackson";
	private static CBORFactory CBOR_FACTORY = new CBORFactory();

	/**
	 * Obtains a {@link Stream} of events from the given file.
	 * 
	 * <p>
	 * Stream operations may throw an {@link UncheckedIOException} or a
	 * {@link RuntimeException} if an event fails to be read from the stream.
	 * 
	 * @param filename
	 *            the name of the file to read the events from
	 * @param eventType
	 *            the type of events to be read
	 * @param <T>
	 *            the type of events to read
	 * @return the stream
	 * @throws IOException
	 *             If an I/O error occurs while creating the stream.
	 */
	public static <T> Stream<T> readEventsFrom(final String filename, final Class<T> eventType) throws IOException {
		return readEventsFrom(new File(filename), eventType);
	}

	/**
	 * Obtains a {@link Stream} of events from the given file.
	 * 
	 * <p>
	 * Stream operations may throw an {@link UncheckedIOException} or a
	 * {@link RuntimeException} if an event fails to be read from the stream.
	 * 
	 * @param file
	 *            the file to read the events from
	 * @param eventType
	 *            the type of events to be read
	 * @param <T>
	 *            the type of events to read
	 * @return the stream
	 * @throws IOException
	 *             If an I/O error occurs while creating the stream.
	 */
	public static <T> Stream<T> readEventsFrom(final File file, final Class<T> eventType) throws IOException {
		return readEventsFrom(file, eventType, emptyMap());
	}

	/**
	 * Obtains a {@link Stream} of events from the given file.
	 * 
	 * <p>
	 * Stream operations may throw an {@link UncheckedIOException} or a
	 * {@link RuntimeException} if an event fails to be read from the stream.
	 * 
	 * @param file
	 *            the file to read the events from
	 * @param eventType
	 *            the type of events to be read
	 * @param mixinConfiguration
	 *            The values of the map configure
	 *            <a href="http://wiki.fasterxml.com/JacksonMixInAnnotations">
	 *            Jackson mixin annotations</a> for the keys of the map, e.g.
	 *            using a
	 *            <a href="http://wiki.fasterxml.com/JacksonAnnotations">Jackson
	 *            annotation</a> to specify which concrete class to use when
	 *            deserializing an instance of an interface or abstract class.
	 * @param <T>
	 *            the type of events to read
	 * @return the stream
	 * @throws IOException
	 *             If an I/O error occurs while creating the stream.
	 */
	public static <T> Stream<T> readEventsFrom(final File file, final Class<T> eventType,
			final Map<Class<?>, Class<?>> mixinConfiguration) throws IOException {
		final InputStream inputStream = new FileInputStream(file);
		final boolean parallel = false;
		final EventSpliterator<T> spliterator = new EventSpliterator<T>(inputStream, eventType, mixinConfiguration);
		return StreamSupport.stream(spliterator, parallel).onClose(spliterator::close);
	}

	/**
	 * Used to create an {@link ObjectOutputStream} for writing events to disk.
	 * 
	 * <p>
	 * If the file whose name is passed to this method does not exist, or if a
	 * directory that directly or indirectly contains the file does not exist,
	 * they are created before this method returns.
	 * 
	 * <p>
	 * The returned stream needs to be closed after use.
	 * 
	 * @param filename
	 *            the name of the file to write events to
	 * @param eventType
	 *            the type of events to be written
	 * @param <T>
	 *            the type of events to write
	 * @return the output to write events to. The output needs to be closed
	 *         after use.
	 * @throws IOException
	 *             If an I/O error occurs while creating the stream.
	 */
	public static <T> EventOutput<T> writeEventsTo(final String filename, final Class<T> eventType) throws IOException {
		return writeEventsTo(new File(filename), eventType);
	}

	/**
	 * Used to create an {@link ObjectOutputStream} for writing events to disk.
	 * 
	 * <p>
	 * If the file whose name is passed to this method does not exist, or if a
	 * directory that directly or indirectly contains the file does not exist,
	 * they are created before this method returns.
	 * 
	 * <p>
	 * The returned stream needs to be closed after use.
	 * 
	 * @param file
	 *            the file to write events to
	 * @param eventType
	 *            the type of events to be written
	 * @param <T>
	 *            the type of events to output
	 * @return the output to write events to. The output needs to be closed
	 *         after use.
	 * @throws IOException
	 *             If an I/O error occurs while creating the stream.
	 * @see #writeEventsTo(File, Class, Map)
	 */
	public static <T> EventOutput<T> writeEventsTo(final File file, final Class<T> eventType) throws IOException {
		return writeEventsTo(file, eventType, emptyMap());
	}

	/**
	 * Used to create an {@link ObjectOutputStream} for writing events to disk.
	 * 
	 * <p>
	 * If the file whose name is passed to this method does not exist, or if a
	 * directory that directly or indirectly contains the file does not exist,
	 * they are created before this method returns.
	 * 
	 * <p>
	 * The returned stream needs to be closed after use.
	 * 
	 * @param file
	 *            the file to write events to
	 * @param eventType
	 *            the type of events to be written
	 * @param mixinConfiguration
	 *            The values of the map configure
	 *            <a href="http://wiki.fasterxml.com/JacksonMixInAnnotations">
	 *            Jackson mixin annotations</a> for the keys of the map, e.g.
	 *            using a
	 *            <a href="http://wiki.fasterxml.com/JacksonAnnotations">Jackson
	 *            annotation</a>.
	 * @param <T>
	 *            the type of events to output
	 * @return the output to write events to. The output needs to be closed
	 *         after use.
	 * @throws IOException
	 *             If an I/O error occurs while creating the stream.
	 */
	public static <T> EventOutput<T> writeEventsTo(final File file, final Class<T> eventType,
			final Map<Class<?>, Class<?>> mixinConfiguration) throws IOException {
		createParentDirectoriesIfNecessary(file);
		return new EventOutputImpl<T>(new FileOutputStream(file), eventType, mixinConfiguration);
	}

	private static void createParentDirectoriesIfNecessary(final File file) {
		final File parentDirectory = file.getParentFile();
		if (parentDirectory != null && !parentDirectory.exists()) {
			parentDirectory.mkdirs();
		}
	}

	private static ObjectMapper createObjectMapper(final Map<Class<?>, Class<?>> mixinConfiguration) {
		final ObjectMapper objectMapper = new ObjectMapper(CBOR_FACTORY);
		mixinConfiguration.entrySet().stream().filter(EventIO::validateMixinConfig)
				.forEach(mixinConfig -> objectMapper.addMixIn(mixinConfig.getKey(), mixinConfig.getValue()));
		return objectMapper;
	}

	private static boolean validateMixinConfig(final Map.Entry<Class<?>, Class<?>> mixinConfig) {
		boolean mixinAnnotationFound = false;
		final Class<?> mixinClass = mixinConfig.getValue();
		for (final Annotation annotation : mixinClass.getAnnotations()) {
			if (annotation.annotationType().getName().startsWith(COM_FASTERXML_JACKSON)) {
				mixinAnnotationFound = true;
			}
		}
		if (!mixinAnnotationFound) {
			throw new IllegalArgumentException("Mixin class " + mixinClass.getName()
					+ " contains no mixin annotation (from package " + COM_FASTERXML_JACKSON + " or below).");
		}
		return mixinAnnotationFound;
	}

	/**
	 * A spliterator for events read from an {@link ObjectInputStream} that ends
	 * iterating after reading a {@link NoFurtherEvents} object.
	 *
	 * @param <T>
	 *            the type of events supplied by this spliterator
	 */
	private static class EventSpliterator<T> extends AbstractSpliterator<T> implements AutoCloseable {

		private final MappingIterator<T> mappingIterator;

		protected EventSpliterator(final InputStream is, final Class<T> eventType,
				final Map<Class<?>, Class<?>> mixinConfiguration) throws IOException {
			super(Long.MAX_VALUE, Spliterator.ORDERED);
			mappingIterator = createMappingIterator(is, eventType, mixinConfiguration);
		}

		private MappingIterator<T> createMappingIterator(final InputStream is, final Class<T> eventType,
				final Map<Class<?>, Class<?>> mixinConfiguration) throws IOException, JsonProcessingException {
			final ObjectMapper objectMapper = createObjectMapper(mixinConfiguration);
			final ObjectReader reader = objectMapper.readerFor(eventType);
			return reader.readValues(is);
		}

		@Override
		public boolean tryAdvance(final Consumer<? super T> action) {
			if (mappingIterator.hasNext()) {
				final T next = mappingIterator.next();
				action.accept(next);
				return true;
			}
			return false;
		}

		@Override
		public void close() {
			try {
				mappingIterator.close();
			} catch (IOException e) {
				throw new UncheckedIOException(e);
			}
		}

	}

	private static class EventOutputImpl<T> implements EventOutput<T> {

		private final SequenceWriter sequenceWriter;

		public EventOutputImpl(final OutputStream outputStream, final Class<T> eventType,
				final Map<Class<?>, Class<?>> mixinConfiguration) throws IOException {
			sequenceWriter = createSequenceWriter(outputStream, eventType, mixinConfiguration);
		}

		private SequenceWriter createSequenceWriter(final OutputStream outputStream, final Class<T> eventType,
				final Map<Class<?>, Class<?>> mixinConfiguration) throws IOException {
			final ObjectMapper objectMapper = createObjectMapper(mixinConfiguration);
			final ObjectWriter writer = objectMapper.writerFor(eventType);
			return writer.writeValues(outputStream);
		}

		@Override
		public void close() throws IOException {
			sequenceWriter.flush();
			sequenceWriter.close();
		}

		@Override
		public void write(final T event) throws IOException {
			Objects.requireNonNull(event, "Event must not be null");
			sequenceWriter.write(event);
		}

	}

}
