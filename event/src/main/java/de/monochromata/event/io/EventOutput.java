package de.monochromata.event.io;

import java.io.IOException;

/**
 * An interface for persisting events.
 * 
 * <p>
 * Implementations of this interface are not required to be thread-safe.
 * 
 * @param <T>
 *            the type of the events to be persisted.
 */
public interface EventOutput<T> extends AutoCloseable {

	/**
	 * Persists the given event
	 * 
	 * @param event
	 *            the event to persist.
	 * @throws IOException
	 *             If the event could not be persisted.
	 * @throws NullPointerException
	 *             If event is {@code null}
	 */
	public void write(T event) throws IOException;

	/**
	 * Closes the event output.
	 * 
	 * @throws IOException
	 *             If the event output cannot be closed.
	 */
	@Override
	public void close() throws IOException;

}
