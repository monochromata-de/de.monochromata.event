package de.monochromata.event;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;

/**
 * An abstract base class for tracking events emitted by third parties and for
 * informing listeners about these events.
 * 
 * @param <L>
 *            the type of listeners that can be attached to the tracker.
 */
public abstract class AbstractTracker<L> implements Tracker<L> {

	private final List<L> listeners = new ArrayList<>();

	/**
	 * Notify all listeners using a given notifier lambda that contains or
	 * creates an event to be sent.
	 */
	protected void notifyListeners(final Consumer<L> notifier) {
		listeners.forEach(listener -> notifier.accept(listener));
	}

	@Override
	public void addListener(final L listener) {
		listeners.add(listener);
	}

	@Override
	public void removeListener(final L listener) {
		listeners.remove(listener);
	}

}
