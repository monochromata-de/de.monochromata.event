# Release 1.2.127

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.9

# Release 1.2.126

* Update dependency org.mockito:mockito-core to v4.3.1

# Release 1.2.125

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.3

# Release 1.2.124

* Update dependency org.mockito:mockito-core to v4.3.0

# Release 1.2.123

* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.9.0

# Release 1.2.122

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.2

# Release 1.2.121

* Update dependency org.reficio:p2-maven-plugin to v2

# Release 1.2.120

* Update dependency org.assertj:assertj-core to v3.22.0

# Release 1.2.119

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.10.0

# Release 1.2.118

* Update jackson.version to v2.13.1

# Release 1.2.117

* Update dependency org.mockito:mockito-core to v4.2.0

# Release 1.2.116

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8.1

# Release 1.2.115

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.8

# Release 1.2.114

* Update dependency org.mockito:mockito-core to v4.1.0

# Release 1.2.113

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.2

# Release 1.2.112

* Update dependency org.mockito:mockito-core to v4

# Release 1.2.111

* Update dependency com.fasterxml.jackson.core:jackson-annotations to v2.13.0

# Release 1.2.110

* Update dependency org.assertj:assertj-core to v3.21.0

# Release 1.2.109

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.1

# Release 1.2.108

* Update dependency com.fasterxml.jackson.core:jackson-annotations to v2.12.5

# Release 1.2.107

* Update dependency org.mockito:mockito-core to v3.12.4

# Release 1.2.106

* Update dependency org.mockito:mockito-core to v3.12.1

# Release 1.2.105

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7.1

# Release 1.2.104

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.7

# Release 1.2.103

* Update jackson.version to v2.12.4

# Release 1.2.102

* Update dependency org.mockito:mockito-core to v3.11.2

# Release 1.2.101

* Update dependency org.assertj:assertj-core to v3.20.2

# Release 1.2.100

* Update dependency org.assertj:assertj-core to v3.20.1

# Release 1.2.99

* Update dependency org.mockito:mockito-core to v3.11.1

# Release 1.2.98

* Update dependency org.mockito:mockito-core to v3.11.0

# Release 1.2.97

* Update dependency org.reficio:p2-maven-plugin to v1.7.0

# Release 1.2.96

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.3.0

# Release 1.2.95

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.6.1

# Release 1.2.94

* Update dependency org.mockito:mockito-core to v3.10.0

# Release 1.2.93

* Update dependency org.jacoco:jacoco-maven-plugin to v0.8.7

# Release 1.2.92

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.2

# Release 1.2.91

* Update jackson.version to v2.12.3

# Release 1.2.90

* Update dependency org.reficio:p2-maven-plugin to v1.6.0

# Release 1.2.89

* Update dependency org.mockito:mockito-core to v3.9.0

# Release 1.2.88

* Update dependency com.fasterxml.jackson.core:jackson-annotations to v2.12.2

# Release 1.2.87

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.5

# Release 1.2.86

* Update dependency org.mockito:mockito-core to v3.8.0

# Release 1.2.85

* Update dependency junit:junit to v4.13.2

# Release 1.2.84

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.4

# Release 1.2.83

* Update dependency org.assertj:assertj-core to v3.19.0

# Release 1.2.82

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.2

# Release 1.2.81

* Update dependency org.mockito:mockito-core to v3.7.7

# Release 1.2.80

* Update jackson.version to v2.12.1

# Release 1.2.79

* Update dependency org.mockito:mockito-core to v3.7.0

# Release 1.2.78

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5.1

# Release 1.2.77

* Publish parent POM for downstream projects

# Release 1.2.76

* No changes

# Release 1.2.75

* No changes

# Release 1.2.74

* Revert to p2-maven-plugin 1.3.0
* Skip site deployment
* Publish m2 and p2 repos to GitLab Pages
* Use p2-maven-plugin:1.5.0 from the Central repository

# Release 1.2.73

* Update dependency org.mockito:mockito-core to v3.6.0

# Release 1.2.72

* Update dependency org.assertj:assertj-core to v3.18.0

# Release 1.2.71

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.5

# Release 1.2.70

* Update dependency org.mockito:mockito-core to v3.5.15

# Release 1.2.69

* Update dependency junit:junit to v4.13.1

# Release 1.2.68

* Update jackson.version to v2.11.3

# Release 1.2.67

* Update dependency org.mockito:mockito-core to v3.5.13

# Release 1.2.66

* Update dependency org.mockito:mockito-core to v3.5.11

# Release 1.2.65

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.3

# Release 1.2.64

* Update dependency org.jacoco:jacoco-maven-plugin to v0.8.6

# Release 1.2.63

* Update dependency org.assertj:assertj-core to v3.17.2

# Release 1.2.62

* Update dependency org.mockito:mockito-core to v3.5.10

# Release 1.2.61

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.1

# Release 1.2.60

* Update dependency org.mockito:mockito-core to v3.5.9

# Release 1.2.59

* Update dependency org.assertj:assertj-core to v3.17.1

# Release 1.2.58

* Update dependency org.mockito:mockito-core to v3.5.7

# Release 1.2.57

* Update dependency org.assertj:assertj-core to v3.17.0

# Release 1.2.56

* Update dependency org.mockito:mockito-core to v3.5.5

# Release 1.2.55

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.2

# Release 1.2.54

* Update dependency org.mockito:mockito-core to v3.5.2

# Release 1.2.53

* Update dependency org.mockito:mockito-core to v3.5.0

# Release 1.2.52

* Update dependency org.codehaus.mojo:versions-maven-plugin to v2.8.1

# Release 1.2.51

* Update jackson.version to v2.11.2

# Release 1.2.50

* Update dependency org.mockito:mockito-core to v3.4.6

# Release 1.2.49

* Update dependency org.mockito:mockito-core to v3.4.4

# Release 1.2.48

* Update dependency org.mockito:mockito-core to v3.4.3

# Release 1.2.47

* Update dependency org.mockito:mockito-core to v3.4.0

# Release 1.2.46

* Update jackson.version to v2.11.1

# Release 1.2.45

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.1

# Release 1.2.44

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4.1

# Release 1.2.43

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.4

# Release 1.2.42

* Update dependency org.apache.maven.plugins:maven-project-info-reports-plugin to v3.1.0

# Release 1.2.41

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.3

# Release 1.2.40

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.2

# Release 1.2.39

* Update dependency org.assertj:assertj-core to v3.16.1

# Release 1.2.38

* Update dependency org.assertj:assertj-core to v3.16.0

# Release 1.2.37

* Update jackson.version to v2.11.0

# Release 1.2.36

* Update dependency org.apache.maven.wagon:wagon-ssh to v3.4.0

# Release 1.2.35

* Update dependency org.apache.maven.plugins:maven-javadoc-plugin to v3.2.0

# Release 1.2.34

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.13

# Release 1.2.33

* Update dependency org.mockito:mockito-core to v3.3.3

# Release 1.2.32

* Update dependency org.apache.maven.plugins:maven-site-plugin to v3.9.0

# Release 1.2.31

* Update jackson.version to v2.10.3

# Release 1.2.30

* Update dependency org.mockito:mockito-core to v3.3.0

# Release 1.2.29

* Update dependency org.assertj:assertj-core to v3.15.0

# Release 1.2.28

* Update dependency nl.jqno.equalsverifier:equalsverifier to v3.1.12

# Release 1.2.27

* No changes

# Release 1.2.26

* No changes

# Release 1.2.25

* No changes

# Release 1.2.24

* No changes

# Release 1.2.23

* No changes

# Release 1.2.22

* No changes

# Release 1.2.21

* No changes

# Release 1.2.20

* No changes

# Release 1.2.19

* No changes

# Release 1.2.18

* No changes

# Release 1.2.17

* No changes

# Release 1.2.16

* No changes

# Release 1.2.15

* No changes

# Release 1.2.14

* No changes

# Release 1.2.13

* No changes

# Release 1.2.12

* No changes

