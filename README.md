# A minimal event-processing framework

[![pipeline status](https://gitlab.com/de.monochromata/de.monochromata.event/badges/master/pipeline.svg)](https://gitlab.com/de.monochromata/de.monochromata.event/commits/master)

* It is available from a Maven repository at https://monochromata-de.gitlab.io/de.monochromata.event/m2/ .
* JavaDoc is available at https://monochromata-de.gitlab.io/de.monochromata.event/apidocs/index.html .
* The Maven site is available at https://monochromata-de.gitlab.io/de.monochromata.event/ .
* An Eclipse p2 repository is available at https://monochromata-de.gitlab.io/de.monochromata.event/p2repo/ .

## Links

The project is used by
[de.monochromata.eyetracking](https://gitlab.com/monochromata/de.monochromata.eyetracking/).

## License

LGPL
